Overview
--------
This module is build specifically for Drupal for Facebook module.

The advantage of subscribing to updates is that you can cache data (like user's
friends list etc.) and thus saving some expensive requests to Facebook.

Through this module you can only subscribe your application(s) and it will
listen for updates but will not update your data for you. For example if you
want to cache user's friends list you can subscribe your application to this
field in user object. You'll receive an update from Facebook moments after a
user (or more) updates his friends list. You'll have to implement hook_fb_live
to do the actual retrieving of the list when the user logs in or when ever you
want. The hook is executed when a fb update is received.

Installation and configuration:
-------------------------------
- Read this first: http://developers.facebook.com/docs/reference/api/realtime/
- Make sure you have fb (Drupal for Facebook) module enabled.
- Copy the module into the modules subdirectory and enable it.
- Go to your Facebook application config path
(admin/structure/fb/app/[app_label]/fb_app)
- Click on Live-update tab and select the fields you want your application to
subscribe to
- Submit the form
Note: You shouldn't need to change the verify-token as it is randomly created
at module install. This field might be hidden in the future.

HowTo:
------
The module has two operations you can hook into by implementing hook_fb_live:

hook_fb_live($op, $data, &$return);

$op = FB_LIVE_OP_SUBSCRIBE - which is triggered right before submitting the
data to facebook (object and fields). You can act here on the field list that
is about to be submitted.

$op = FB_LIVE_OP_UPDATE - which is triggered in the listener function (when
facebook sends you an update). Attention!!! If you want to process things
here take care to not do it for longer than 15 sec. Being a call made by
Facebook to your app, they set this timeout in order to save their servers from
useless lags. You clear the cache here and when a new request is made your
function would detect the empty cache and request the new list of friends and
cache it again.

Example:
========
<?php

/**
 * Implements hook_fb_live().
 */
function mymodule_fb_live($op, $data, &$return) {

  switch ($op) {
    case FB_LIVE_OP_UPDATE_RECEIVED:
      // We received an update event so we clear the caches for the fields we
      // got in the update.
      switch ($data['object']) {
        case 'user':
          foreach ($data['entry'] as $entry) {
            if (in_array('friends', $entry['changed_fields'])) {
              // That's FB's uid not Drupal's.
              $fbu = $entry['uid'];
              cache_clear_all('friends-of:' . $fbu);
            }
          }
          break;

        case 'permissions':
          foreach ($data['entry'] as $entry) {
            if (in_array('publish_stream', $entry['changed_fields'])) {
              $fbu = $entry['uid'];
              cache_clear_all('permissions-of:' . $fbu);
            }
          }
          break;
      }
      break;
  }
}

/**
 * Helper function to get cached facebook friends list.
 *
 * @param bool $force_recache
 *   forces an api call to retrieve the friends list
 *
 * @return array
 *   array of friends list on success or boolean FALSE otherwise
 */
function fb_get_friends($force_recache = FALSE) {
  // Only to get the curent $fbu.
  extract(fb_vars());
  // Let's try to load it from cache.
  $cache = cache_get('fb_friends_of:' . $fbu);

  if (!$cache || $force_recache) {
    try {
      // We have to update the friends list.
      $query = "SELECT uid, name, pic_square, can_post FROM user WHERE uid IN
        (SELECT uid2 FROM friend WHERE uid1 = $fbu) ORDER BY name ASC";
      $friends_list = fb_fql_query($fb, $query);
    }
    catch (FacebookApiException $e) {
      // Couldn't connect to Facebook or something bad happend.
      fb_log_exception($e, t('Failed to load friends.<br /><pre>%params</pre>',
          array('%params' => $params)), $fb);
      return FALSE;
    }

    if (isset($friends_list)) {
      // We got the friends list so it's time to caceh it.
      cache_set('fb_friends_of:' . $fbu, $friends_list);
    }
  }
  else {
    // No update is needed and the list is already cached so we simply return
    // the cached list.
    $friends_list = $cache->data;
  }

  return $friends_list;
}
